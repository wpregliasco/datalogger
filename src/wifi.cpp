// Wifi
// init:
//    si faltan datos para la conexión 
//    o se mantiene presionado el botón RESET durante el arranque
//    entonces
//    se inicia un servidor para ingresar esos datos
//    en caso conrario,
//    intenta conectarse por tiempo indeterminado
//
//    bloquea todo hasta lograr una conexión. 
// luego del init, no hay interrupción si se desconecta.

#ifdef DEBUG
#define OUT_INIT Serial.begin(115200)
#define OUT(x)   Serial.print(x)
#define OUTLN(x) Serial.println(x)
#else
#define OUT_INIT ;
#define OUT(x)   ;
#define OUTLN(x) ;
#endif

#include "LittleFS.h"
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>

#include "wifi.h"

Wifi::Wifi()
{
    load_config();
}

void Wifi::init(void)
{
    ETS_UART_INTR_DISABLE();
    wifi_station_disconnect();
    ETS_UART_INTR_ENABLE();
    WiFi.begin();
}

int Wifi::not_stored_data(void){
    return (WiFi.SSID() == "" or !is_config);
}

void Wifi::portal(char const *portalname)
{
    // read config
    char TSChannel_s[1024];
    sprintf( TSChannel_s, "%lu", tspeak_Channel);
    char loop_interval_s[256];
    sprintf( loop_interval_s, "%lu", loop_interval);

    WiFiManagerParameter custom_interval("Loop Interval", "Loop Interval", loop_interval_s, 10);
    WiFiManagerParameter custom_channel("TSpeak Channel", "TSpeak Channel", TSChannel_s, 10);
    WiFiManagerParameter custom_key("TSpeak Key", "TSpeak Key", tspeak_APIKey, 20);

    wifiManager.addParameter(&custom_interval);
    wifiManager.addParameter(&custom_channel);
    wifiManager.addParameter(&custom_key);

    // portal
    wifiManager.startConfigPortal(portalname);

    // save config
    char *pEnd;
    loop_interval = strtoul(custom_interval.getValue(),&pEnd,10);
    tspeak_Channel= strtoul(custom_channel.getValue(), &pEnd,10);
    strcpy(tspeak_APIKey, custom_key.getValue());

    OUTLN("      New value:   " + String(loop_interval));
    OUTLN("      New value:   " + String(tspeak_Channel));
    OUTLN("      New value:   " + String(tspeak_APIKey));
    save_config();
}

int Wifi::isConnected(void)
{
    return (WiFi.status() == WL_CONNECTED);
}

void Wifi::load_config(void)
{
   // Montaje del disco
   OUT("[wifi] mounting FS...");
   if (LittleFS.begin()){
        OUTLN("mounted."); 
   } 

   // Leer archivo `config.json`
   if (LittleFS.exists("/config.json")) {
      OUT("[wifi] reading config file...");
      File configFile = LittleFS.open("/config.json", "r");
      if (configFile) {
          OUTLN("open.");

          // Allocate a buffer
          size_t size = configFile.size();

          // read Json
          configFile.readBytes(buffer, size);
          DynamicJsonDocument doc(1024);
          deserializeJson(doc, buffer);

          loop_interval  = doc["loop_interval"];
          tspeak_Channel = doc["tspeak_channel"];
          strcpy(tspeak_APIKey, doc["tspeak_APIKey"]);
          OUTLN("l      loop_int : "+ String(loop_interval));
          OUTLN("l      tschannel: "+ String(tspeak_Channel));
          OUTLN("l      tsAPIKey : "+ String(tspeak_APIKey));

      };
 
      configFile.close();
      is_config = 1;
    }
    else{
        OUTLN("[wifi] `config.json` file not found.");
        is_config = 0;
    }
}

void Wifi::save_config(void){
    DynamicJsonDocument doc(1024);
    doc["loop_interval"]  = loop_interval;
    doc["tspeak_channel"] = tspeak_Channel;
    doc["tspeak_APIKey"]  = tspeak_APIKey;
    serializeJson(doc, buffer);

    File outfile = LittleFS.open("/config.json", "w"); 
    outfile.print(buffer);
    outfile.close();
}