// AHT10: Temperatura y Humedad

#include <Arduino.h>
#include <Adafruit_AHTX0.h>
#include "temp.h"

void Temperatura::init(int32_t addr)
{
    address = addr;
    if (begin(&Wire, address)){
        Serial.println("[temp] Sensor de temperatura AHT10: inicializado." );
    } else{
        Serial.println("[temp] Sensor de temperatura AHT10: not found." );
    }
}

void Temperatura::read(void)
{
    getEvent(&hum, &temp); // populate temp and humidity objects with fresh data

    temperature = temp.temperature;
    humidity = hum.relative_humidity;
}