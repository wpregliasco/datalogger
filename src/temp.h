#include <Arduino.h>
#include <Adafruit_AHTX0.h>


class Temperatura : public Adafruit_AHTX0
{
    uint32_t address;
    sensors_event_t hum, temp;
    
    public:
        void init(int32_t addr);
        void read(void);

        float temperature, humidity;

};