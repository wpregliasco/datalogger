// Reset Button
// Si está presionado en el inicio durante 3s, 
// dispara el menú de configuración
// Sólo se pueden usar los pines D5, D6, D7
// Cableado:
//     cable 1: Di 
//     cable 2: GND

#include <Arduino.h>
#include "resetBtn.h"

void ResetBtn::init(int reset_pin){
    pinMode(reset_pin, INPUT_PULLUP);
    pin = reset_pin;
}

boolean ResetBtn::status(void){
    return digitalRead(pin)==LOW;
}

boolean ResetBtn::longPress(void){
    // Lee el estado del botón en dos momentos. 
    // Si en ambos está presionado, lo da por presionado.
    delay(1000);
    if (ResetBtn::status()){
        delay(1000);
        if (ResetBtn::status()){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}