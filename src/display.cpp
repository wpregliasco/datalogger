// // Display
// //
// // declaración:
// //    el constructor hereda la clase Adafruit_SSD1306
// //
// // init
// //

#ifdef DEBUG
#define OUT_INIT Serial.begin(115200)
#define OUT(x)   Serial.print(x)
#define OUTLN(x) Serial.println(x)
#else
#define OUT_INIT ;
#define OUT(x)   ;
#define OUTLN(x) ;
#endif

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "display.h"

void Display::init(void)
{
    if(!begin(SSD1306_SWITCHCAPVCC, addr)) {
        OUTLN("Oled Display: SSD1306 allocation failed");
        reinit();
        if(!begin(SSD1306_SWITCHCAPVCC, addr)) {
            OUTLN("Oled Display: SSD1306 allocation failed");
            for(;;);
        }
    };
}

void Display::reinit(void)
{
    //TWRC = 0;
    Wire.begin();
    init();
}
