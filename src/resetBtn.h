class ResetBtn
{
    int pin;
    public:
        void init(int reset_pin);
        boolean status(void);
        boolean longPress(void);
};