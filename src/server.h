#include <ESP8266WiFi.h>
#include "sdmem.h"

class Webserver : public WiFiServer
{
    SDmem* sd;
    long timeoutTime;
    String pname;

    public:
        Webserver () : WiFiServer {80}{
        };
        void init(int seconds, String portal_name, SDmem* sdmem){
            timeoutTime = seconds*1000;
            pname = portal_name;
            sd = sdmem;
            begin();
        };
        void activate(void);

        String header;
        WiFiClient client;
        String html(String file);
};