// v1.5


#ifdef DEBUG
#define OUT_INIT  Serial.begin(115200)
#define OUT(x)    Serial.print(x)
#define OUTLN(x)  Serial.println(x)
#define OUTF(f,x) Serial.println(f,x)
#else
#define OUT_INIT ;
#define OUT(x)   ;
#define OUTLN(x) ;
#endif

#ifdef LCD_DISPLAY
#define LCD(x) x
#else
#define LCD(x) ;
#endif

// headers
#include <Arduino.h>
#include <string.h>
#include "resetBtn.h"
#include "ipBtn.h"
#include "wifi.h"
#include "server.h"
#include "reloj.h"
#include "display.h"
#include "tspeak.h"
#include "sdmem.h"
#include "temp.h"
#include "luz.h"

void(* resetFunc) (void) = 0; //para poder hacer RESET
void welcome_msg(void);
void acq(void);

// Conexiones
#define PIN_I2C_SCL D1   //no conviene cambiarlos
#define PIN_I2C_SDA D2   //no conviene cambiarlos
#define PIN_DHT     D3   //temperatura y humedad (con D0 no anda)
#define PIN_RESET   D2   //D5, D6, D7
#define PIN_IP      D4
// const uint8_t SOFT_MISO_PIN = 12;
// const uint8_t SOFT_MOSI_PIN = 13;
// const uint8_t SOFT_SCK_PIN  = 14;
// const uint8_t SD_CS_PIN     = 15;

// Variables
#define PORTAL_NAME "dataL02"
#define DISPLAY_ADDR   0x3C    //Oled
#define TEMP_ADDR      0x38    //AHT10
#define LIGHT_ADDR     0x29    //TSL2591
#define SCREEN_WIDTH    128
#define SCREEN_HEIGHT    32
#define GMT              -3

unsigned long loop_interval = 15, loop_align = 15;
ResetBtn resetBtn;
IPBtn ipBtn;
Wifi wifi;
Webserver server;
Reloj reloj;
Thingspeak thingspeak;
SDmem sdmem;
Display display(DISPLAY_ADDR, SCREEN_WIDTH, SCREEN_HEIGHT, D0);
Temperatura temp;
Luz luz;    // ganancia auto, integración 500ms

//---------------------------------------------------------------//
class AcqData{
  public:
    // datos originales
    String datetime;
    float TH[2];
    float P;
    float TP;
    float LIR;
    float LFULL;
    float LLUX;
    boolean wifiStatus;
    boolean tspeakStatus;
    // datos modificados
    char buffer1[20];
    char buffer2[20];
    String DATEs;
    String TIMEs;
    float T;
    float H;
    String Ts;
    String Tu = "C";
    String Hs;
    String Hu = "%";
    String Ps;
    String Pu = "hPa";
    String TPs;
    String TPu = "C";
    String LIRs;
    String LFULLs;
    String LLUXs;
    String LLUXu = "lux";
    String WIFIsd;
    String WIFIso;
    String TSPsd;
    String TSPso;

    // sets
    void setDatetime(String dtime){
      // sets datetime, TIMEs, DATEs
      datetime = dtime;
      dtime.toCharArray(buffer1, 20);
      strcpy(buffer2 , &buffer1[11]);
      TIMEs = String(buffer2);
      buffer1[10]='\0';
      strcpy(buffer2 , buffer1);
      DATEs = String(buffer2);
    };
    void setTH(float tc, float hr){
      T = tc;
      sprintf(buffer1, "%.2f", T);
      Ts = String(buffer1);

      H = hr;
      sprintf(buffer1, "%.2f", H);
      Hs = String(buffer1);
    };
    void setP(float p){
      P = p/100.0F;
      sprintf(buffer1, "%.2f", P);
      Ps = String(buffer1);
    }
    void setTP(float t){
      TP = t;
      sprintf(buffer1, "%.2f", TP);
      TPs = String(buffer1);
    }
    void setLuz(uint16_t ir, uint16_t full, float lum, int factor){
      LIR   = (float)ir*(float)factor;
      LIRs = String(LIR);
      LFULL = (float)full*(float)factor;
      LFULLs = String(LFULL);
      LLUX  = lum;
      sprintf(buffer1, "%.3f", LLUX);
      LLUXs = String(buffer1);
    }
    void setWifiStatus(int is_connected){
    wifiStatus = is_connected;
    if (wifiStatus){
      WIFIsd = "W";
      WIFIso = "WiFi On";
    } else{
      WIFIsd = "-";
      WIFIso = "no WiFi :-p";
    }
  };
    void setTSpeak(bool status){
    tspeakStatus = status;
    if (status){
      TSPsd = "T";
      TSPso = "Uploaded.";
    } else{
      TSPsd = "-";
      TSPso = "Problem uploading";
    }
  };

  // outs
  String intS(String s){
    return String(s.toInt());
  }
  String outString(void){
  
    return "  t: " +Ts +Tu +
           "  h: " +Hs +Hu +
           //"  tp: "+TPs+TPu+
           //"  p: "  +Ps +Pu +
           "  luz:" +LLUXs+LLUXu+" ("+LIRs+"/"+LFULLs+")"+
           "   "
           ;
  };
  String outWifi(void){
    return WIFIso;
  };
  String outSd(void){
    return datetime+", "+Ts+", "+Hs+", "
                   //+Ps+", "+TPs+", "
                   +LIRs+", "+LFULLs+", "+LLUXs+", "
                   +String(wifiStatus)+", "+String(tspeakStatus);
  };
  String outLCD1(void){
    return TIMEs+ "           " +WIFIsd+TSPsd;
  };
  String outLCD2(void){
    return "   "+Ts+Tu+"      "+intS(Hs)+Hu+"\n   "
                ///+intS(Ps)+"."+Pu+"   "
                +intS(LLUXs)+"."+LLUXu;
  };
} data;
//---------------------------------------------------------------//
class Oled_msg{
  public:
    void logo(void){
      delay(300);
      display.display();
      delay(500);
    };
    void init_wifi(void){
      display.clearDisplay();
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.print(".Init WiFi");
      display.display();
      display.startscrollright(0x00, 0x0F);
    }
    void wifi_ok(void){
      display.stopscroll();
      display.clearDisplay();
      display.setCursor(0,0);
      display.println("<WiFi: oK>");
      display.display();
    };
    void init_hora(void){
      display.setCursor(0,16);
      display.println(".hora..");
      display.display();
      //display.startscrollright(0x00, 0x0F);
    };
    void hora_ok(void){
      //display.stopscroll();
      display.setTextColor(BLACK);
      display.setCursor(0,16);
      display.println(".hora..");
      display.setCursor(0,16);
      display.setTextColor(WHITE);
      display.println("<Hora: oK>  ");
      display.display();
    };
    void waiting(void){
      display.clearDisplay();
      display.setTextSize(1);
      display.setCursor(0,6);
      display.println("Esperando");
      display.setCursor(0,16);
      display.println("primer dato...");
      display.display();
    };
    void no_net_data(void){
      display.clearDisplay();
      display.setTextSize(1);
      display.setCursor(0,0);
      display.println("Sin datos previos de red");
      display.display();
    };
    void change_net_data(void){
      display.clearDisplay();
      display.setTextSize(1);
      display.setCursor(0,0);
      display.println("Cambiar datos de red");
      display.display();
    };
    void connect_portal(void){
      display.setTextSize(1);
      display.setCursor(0,8);
      display.println("Conectarse a WiFi:");
      display.setTextSize(2);
      display.setCursor(16,16);
      display.println(PORTAL_NAME);
      display.display();
    };
    void progressbar(void){
      int h  = 1;
      int y0 = display.height()-h;
      int w  = (reloj.next_acq_time-reloj.last_read_time)*display.width()/reloj.interval;
      display.fillRect(0, y0, w , h, WHITE);
      display.fillRect(w, y0, display.width()-w , h, BLACK);
      display.display();
    }
} omsg;
//---------------------------------------------------------------//
void setup() 
{  
  welcome_msg();
  resetBtn.init(PIN_RESET);
        LCD(display.init());
        LCD(omsg.logo());

  //// red  y display ////////////////////////////////////////////////
  if (resetBtn.longPress() or wifi.not_stored_data())
  {
    if (wifi.not_stored_data()){
        LCD(omsg.no_net_data());
        OUTLN("[wifi] no hay datos previos");
    } else{
        while (resetBtn.status()){delay(100);}
        LCD(display.reinit());  //el reinit es un buen intento, pero no funciona
        LCD(omsg.logo());
        OUTLN("[wifi] redefiniendo datos de conexión");
    }
    
    OUT("[wifi] conectarse a WiFi SSID: "); OUTLN(PORTAL_NAME);
        //LCD(omsg.connect_portal()); // fue un buen intento
    wifi.portal(PORTAL_NAME);
    resetFunc(); //Necesito resetear porque la conexión I2C está arruinada
  }
  else
  {
    LCD(display.init());
    LCD(omsg.logo());
    OUTLN("[wifi] usando datos almacenados");
    wifi.init();
  }

  // espero la conexión
        OUT("[wifi] conectando");
        LCD(omsg.init_wifi());
  while(!wifi.isConnected()) 
  {
      OUT(".");
      delay(1000);
  }
  // conectado
  loop_interval = wifi.loop_interval;
  loop_align = loop_interval;

  OUTLN("");
  OUTLN("[wifi] conectado.");
  OUT(  "#     Período    :  "); OUT(loop_interval); OUTLN("s");
  OUT(  "#     Alineación :  "); OUT(loop_align);    OUTLN("s");

  wifi.IP = WiFi.localIP().toString();
  OUT("[wifi] IP: "); OUTLN(wifi.IP);
        LCD(omsg.wifi_ok());
  ////////////////////////////////////////////////////////////
  sdmem.init(loop_interval);
  ipBtn.init(PIN_IP);
  thingspeak.init(wifi.tspeak_Channel, wifi.tspeak_APIKey);
        LCD(omsg.init_hora());
  reloj.init(GMT, loop_interval, loop_align);   
        LCD(omsg.hora_ok());
  temp.init(TEMP_ADDR);
  luz.init();
        LCD(omsg.waiting());

  server.init(loop_interval*3/4-1,PORTAL_NAME, &sdmem);
}
//---------------------------------------------------------------//
void loop() {
  if (reloj.is_time_to_acq())
  {
    acq();
    reloj.acq_end();
    ipBtn.reset();
  } 
  else{
    omsg.progressbar();

    if (!ipBtn.pressed and ipBtn.status()){
      ipBtn.activate();
      OUTLN("[ipbtn] Botón IP!");
      display.clearDisplay();
      display.setTextSize(1);
      display.setCursor(0,12);
      display.println(wifi.IP);
      display.display(); 
    }
    server.activate();
    }
}
//---------------------------------------------------------------//
void acq(void)
{ 
  omsg.progressbar();
  // time & sensors reading
      data.setDatetime(reloj.getIsoTime());
      OUT(data.TIMEs); OUT("  >>>");

      temp.read();
      data.setTH(temp.temperature, temp.humidity);

      luz.read_auto();  
      data.setLuz(luz.ir, 
                  luz.full, 
                  luz.luminosity,
                  luz.read_factor());

      OUT(data.outString());

  // publish data
      data.setWifiStatus(wifi.isConnected());
      OUT(data.outWifi());


    if (data.wifiStatus){
      int tout = thingspeak.publish(data.T, data.H, 
                                    //data.TP, data.P,
                                    data.LIR, data.LFULL, data.LLUX);
      if(tout == 200){
        data.setTSpeak(true);
        OUTLN(" / "+data.TSPso);
      }
      else{
        data.setTSpeak(false);
        OUTLN(" / "+data.TSPso+ "("+String(tout)+").");
      }  
    }
    else{
      data.setTSpeak(false);
      OUTLN(" / "+data.TSPso);
    }
  
  // salidas
  sdmem.log(data.DATEs, data.outSd());

  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println(data.outLCD1()); 
  display.setCursor(0,9);
  display.println(data.outLCD2()); 
  omsg.progressbar();
}
//---------------------------------------------------------------//
void welcome_msg(void)
{
    OUT_INIT;
    OUTLN("\n\n\n");
    OUTLN("##################################################");
    OUTLN("#");
    OUTLN("# Datalogger");
    OUTLN("#");
    OUTLN("# por wpregliasco");
    OUTLN("# https://gitlab.com/wpregliasco/datalogger");
    OUTLN("#");
    OUT(  "# Compiled "); OUTLN( __DATE__ " at " __TIME__  );
    OUTLN("##################################################");
    OUTLN("# Conexiones:");
    OUT(  "#     Reset      :  "); OUTLN(PIN_RESET);
    OUT(  "#     IP info    :  "); OUTLN(PIN_IP);
    OUT(  "#     DHT        :  "); OUTLN(PIN_DHT);
    OUT(  "#     SPI  MISO  :  "); OUTLN("12");
    OUT(  "#     SPI  MOSI  :  "); OUTLN("13");
    OUT(  "#     SPI  SCK   :  "); OUTLN("14");
    OUT(  "#     SPI  CS    :  "); OUTLN("15");
    OUTLN("# I2C:");
    OUT(  "#     SCL        :  "); OUTLN(PIN_I2C_SCL);
    OUT(  "#     SDA        :  "); OUTLN(PIN_I2C_SDA);
    OUT(  "#     Display adr:  "); OUTF(DISPLAY_ADDR, HEX);
    OUT(  "#     Display adr:  "); OUTF(TEMP_ADDR, HEX);
    OUT(  "#     Light   adr:  "); OUTF(LIGHT_ADDR, HEX);
    OUTLN("# Variables:");
    OUT(  "#     Red de conf:  "); OUTLN(PORTAL_NAME);
    OUT(  "#     GMT        :  "); OUTLN(GMT);
    OUT(  "#     TS Channel :  "); OUTLN(wifi.tspeak_Channel);
    OUT(  "#     TS Key     :  "); OUTLN(wifi.tspeak_APIKey);
    OUTLN("##################################################");
}
//---------------------------------------------------------------//