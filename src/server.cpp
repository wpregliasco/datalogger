#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "server.h"

void Webserver::activate(){
    client = available();
    // Current & Previous time
    unsigned long currentTime = millis();
    unsigned long previousTime = 0;

    // server
    if (client){
        currentTime = millis();
        previousTime = currentTime;
        Serial.println("[server] New Client."); // print a message out in the serial port
        String currentLine = "";                // make a String to hold incoming data from the client
        while (client.connected() && currentTime - previousTime <= timeoutTime) {  // loop while the client's connected
          currentTime = millis();
          if (client.available()) {             // if there's bytes to read from the client,
 
            char c = client.read();             // read a byte, then
            Serial.write(c);                    // print it out the serial monitor
            header += c;
            if (c == '\n') {                    // if the byte is a newline character

              // if the current line is blank, you got two newline characters in a row.
              // that's the end of the client HTTP request, so send a response:
              if (currentLine.length() == 0) {
                break;
              } else { // if you got a newline, then clear currentLine
                currentLine = "";
              }
            } else if (c != '\r') {  // if you got anything else but a carriage return character,
              currentLine += c;      // add it to the end of the currentLine
            }
           }
        }

        // Display the HTML web page
        client.print(html(header).c_str());

        // Clear the header variable
        header = "";
        // Close the connection
        client.stop();
        Serial.println("Client disconnected.");
        Serial.println("");
    }
}

String Webserver::html(String request){

    int i0 = request.indexOf(" ")+1;
    int i1 = request.indexOf(" ",i0);
    String subdomain = request.substring(i0,i1);

    String out="";
    String lof = sd->ls(), fname;

    String outHeaderHtml = String("HTTP/1.1 200 OK\nContent-type:text/html\nConnection: close\n\n\n");
    
    if (subdomain=="/"){
        out =  outHeaderHtml;
        out += String("<!DOCTYPE html><html>\n");
        out += String("   <head>\n");
        out += String("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
        out += String("      <link rel=\"icon\" href=\"data:,\">\n");
        out += String("      <style>\n");
        out += String("         html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n");
        out += String("      </style>\n");
        out += String("   </head>\n");
        out += String("   <body style='background-color:grey;'>\n");
        out += String("      <H1>DataLogger Web Server</H1>\n");
        out += String("      <H2>") + pname + String("</H2>\n");
        //////////////////////////////////////////////////////////////
        out += String("      <div  align='center' style='margin:auto;pad:auto;'>\n");
        out += String("      <div style='display: inline-block; text-align: left;'>\n");

        i0 = 0;                
        while((i0+1)<(int)lof.length()){
            i1 = lof.indexOf("\n",i0);
            fname = lof.substring(i0,i1);
            i0 = i1+1;
            out += String("<a href='http://"+ WiFi.localIP().toString() + "/" + fname +"'>") + fname + String("</a><br>\n");
        }
        out += String("</div>\n");
        out += String("</div>\n");
        out += String("</body>\n");
        out += String("</html>");
    }
    else{
        // check if subdomain is a valid file
        i0 = 0;         
        int found=0;       
        while((i0+1)<(int)lof.length()){
            i1 = lof.indexOf("\n",i0);
            fname = lof.substring(i0,i1);
            if (fname == subdomain.substring(1)){
                found=1;
                break;
            }
            i0 = i1+1;
        }
        if (found){
            

            //header
            out = "";
            // out  = String("Content-Disposition: attachment; filename=\"") + fname + String("\"\n");
            // out += String("Content-Type: application/force-download\n");
            // out += String("Content-Transfer-Encoding: binary\n");
            // out += String("Content-Length: ") + sd->file.fileSize();  
            // out += "\n\n\n";
            char line[100];
            int n;

            sd->file.open(fname.c_str(), O_READ);  
            sd->file.rewind();

            while ((n = sd->file.fgets(line, sizeof(line))) > 0) {
                client.print(line);
            }
            
            sd->file.close();
            return String("\n");

        } else{
            out =  outHeaderHtml;
            out += String("<!DOCTYPE html><html>\n");
            out += String("   <head>\n");
            out += String("      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
            out += String("      <link rel=\"icon\" href=\"data:,\">\n");
            out += String("      <style>\n");
            out += String("         html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n");
            out += String("      </style>\n");
            out += String("   </head>\n");
            out += String("   <body style='background-color:grey;'>\n");
            out += String("      <H1>DataLogger Web Server</H1>\n");
            out += String("      <H2>data: ") + subdomain + String(", no existe.</H2>\n");
            out += String("      <A HREF=\"http://"+ WiFi.localIP().toString() + "/\"> <H3>listado de archivos</H3></A>\n");
            out += String("</body>");
            out += String("</html>");
        }
    }

    return out;
}