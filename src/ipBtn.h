#include <Arduino.h>

class IPBtn
{
    int pin;

    public:
        void init(int ip_pin);
        boolean status(void);
        boolean longPress(void);
        void activate(void);
        void reset(void);

        int pressed;
};