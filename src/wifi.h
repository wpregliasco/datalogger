#include <WiFiManager.h>
//#include <Arduino.h>
class Wifi
{
    WiFiManager wifiManager;
    void save_config(void);
    void load_config(void);
    int is_config;
    char buffer[1024];

    public:
        Wifi();
        void init(void);
        int  not_stored_data(void);
        void portal(char const *portalname);
        int  isConnected(void);
        
        unsigned long tspeak_Channel;
        unsigned long loop_interval;
        char tspeak_APIKey[20];
        String IP;
};