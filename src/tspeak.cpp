#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"
#include "tspeak.h"

void Thingspeak::init(unsigned long ChannelNumber, const char* WriteAPIKey)
{
    Channel = ChannelNumber;
    Key = WriteAPIKey;
    WiFi.mode(WIFI_STA);   
    ThingSpeak.begin(client); 
}

int Thingspeak::writeField(unsigned int field, float value)
{
    return ThingSpeak.writeField(Channel, field, value, Key);
}

int Thingspeak::publish(float var1, float var2, 
                        //float var3, float var4, 
                        float var5, float var6, 
                        float var7)
{
    ThingSpeak.setField(1, var1);
    ThingSpeak.setField(2, var2);
    ThingSpeak.setField(3, var5);
    ThingSpeak.setField(4, var6);
    ThingSpeak.setField(5, var7);

    return ThingSpeak.writeFields(Channel, Key);
}