#include <NTPClient.h>
#include <WiFiUdp.h>

class Reloj
{
    WiFiUDP ntpUDP;
    NTPClient timeClient{ntpUDP};
    int GMT;
    char buffer[80];
    void set_align_acq(void);
    void set_last_read_time(void);
    void set_next_acq_time(void);

    public:
        void init(long gmt, long acq_interval, long acq_align); 
        String getIsoTime();
        String getIsoTime(long rawtime);
        int is_time_to_acq(void);
        void acq_end(void);

        unsigned long last_read_time;
        unsigned long last_acq_time;
        unsigned long next_acq_time;
        unsigned long interval;
        unsigned long align;
};

// el uso de {} en la declaración de timeClient
// lo saqué de https://www.cplusplus.com/doc/tutorial/classes/
// siempre se aprende algo.