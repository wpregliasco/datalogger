// Reloj
// Realiza la conexión con el servidor NTP 
// y organiza los tiempos de adquisición
//
// init:
//    configura y pone la hora por primera vez en el ESP
//    bloquea la corrida hasta que se conecta y está la información
//    durante el resto de la adquisición, no es necesaria la conexión
//    a la red, a menos que se haga un reset
//    Establece el tiempo de la primer adquisición alineada
//
// set_last_read_time:
//    la lectura de tiempos se hace en un buffer, en lugar de volver a 
//    tomar varias veces el tiempo del sistema para definir un mismo evento
//
// getIsoTime:  
//    2022-03-19 14:52:14
//    si no se ingresa un parámetro de EpochTime, lo hace para el tiempo actual
//
// Funciones de Loop
//
// is_time_to_acq:
//    devuelve true si es hora de hacer una nueva adquisición
// acq_end:
//    hay que ponerlo al final de la adquisición. 
//

#ifdef DEBUG
#define OUT_INIT Serial.begin(115200)
#define OUT(x)   Serial.print(x)
#define OUTLN(x) Serial.println(x)
#else
#define OUT_INIT ;
#define OUT(x)   ;
#define OUTLN(x) ;
#endif

#include <Arduino.h>
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <time.h>

#include "reloj.h"

void Reloj::init(long gmt, long acq_interval, long acq_align)
{
  OUT("[reloj] conectando...");
  GMT = gmt;
  timeClient.setTimeOffset(GMT*3600);

  last_read_time = 0;
  last_acq_time = 0;
  interval = acq_interval;
  align = acq_align;

  timeClient.begin();
  timeClient.update();
  // espero a que haya una fecha razonable
  while(timeClient.getEpochTime() > 4000000000){ //pasado cercano (pasado de rosca)
    delay(10000);
    timeClient.begin();
    timeClient.update();
  }
  OUTLN(" conectado.");

  set_align_acq();
}

void Reloj::set_align_acq(void)
{
  // Establece el primer tiempo proyectado de adquisición
  // para que las mediciones queden alineadas
  unsigned long now_time = timeClient.getEpochTime();

  //   t0 del día
  time_t rawtime = (time_t) now_time;
  tm ts = *localtime(&rawtime);
  ts.tm_hour = 0;
  ts.tm_min = 0;
  ts.tm_sec = 0;
  unsigned long t0_of_day = mktime(&ts);

  // próximo alineado
  next_acq_time = t0_of_day + ((int)((now_time - t0_of_day)/align) + 1) * align;
}

void Reloj::set_next_acq_time(void)
{
  unsigned long now_time = timeClient.getEpochTime();
  
  while (next_acq_time <= now_time)
  {
    next_acq_time += interval;
  }
}

void Reloj::set_last_read_time(void)
{
  last_read_time = timeClient.getEpochTime();
}

String Reloj::getIsoTime(long rawtime)
{
  // Format time, "yyyy-mm-dd hh:mm:ss"
  time_t rawtime2 = (time_t) rawtime;
  tm ts = *localtime(&rawtime2);
  strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &ts);

  return buffer;
}

String Reloj::getIsoTime()
{
  return getIsoTime(last_acq_time);
}

int Reloj::is_time_to_acq()
{
  // returns true it is time for a new acq
  set_last_read_time();
  if (last_read_time >= next_acq_time) 
  {
    last_acq_time = last_read_time;
    return true;
  }
  else
  {
    return  false;
  }
}

void Reloj::acq_end(void)
{
  set_next_acq_time();
  // reconnect with NTP server
  timeClient.update();
}