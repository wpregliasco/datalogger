// Sensor de luz TSL 2591

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"
#include "luz.h"

void Luz::info(void){
  sensor_t sensor;
  getSensor(&sensor);
  
  strcpy(name, sensor.name);
  version = sensor.version;
  sensor_id = sensor.sensor_id;
  max_value = sensor.max_value; //lux
  min_value = sensor.min_value; //lux
  resolution = sensor.resolution; //lux

  Serial.print("[luz] Sensor     "); Serial.println(name);
  Serial.print("      Version    "); Serial.println(version);
  Serial.print("      Min/Max    "); Serial.println(String(min_value)+" / "+String(max_value)+" lux");
  Serial.print("      Resolution "); Serial.println(String(resolution)+" lux");
};

void Luz::init(void){

    Serial.println("[luz] Starting Adafruit TSL2591 Test!");
  
    if (begin()) 
    {
        Serial.println("[luz] Found a TSL2591 sensor");
    } 
    else 
    {
        Serial.println("[luz] No sensor found ... check your wiring!");
        while (1);
    }
        
    // Load and Display some basic information on this sensor
    info();

    ///////// Configuration
    // You can change the gain on the fly, to adapt to brighter/dimmer light situations
    switch(gain)
    {
        case 1:
            setGain(TSL2591_GAIN_LOW); 
            Serial.println("[luz] ganancia: 1x (Low)");           
        break;
        case 2:
            setGain(TSL2591_GAIN_MED);
            Serial.println("[luz] ganancia: 25x (Medium)");
        break;
        case 3:
            setGain(TSL2591_GAIN_HIGH);
            Serial.println("[luz] ganancia: 428x (High)");
        break;
        case 4:
            setGain(TSL2591_GAIN_MAX); 
            Serial.println("[luz] ganancia: 9876x (Max)");
        break;
    }

    // Changing the integration time gives you a longer time over which to sense light
    // longer timelines are slower, but are good in very low light situations!
    switch(integration)
    {
        case 100:
            setTiming(TSL2591_INTEGRATIONTIME_100MS);
            break;
        case 200:
            setTiming(TSL2591_INTEGRATIONTIME_200MS);
            break;
        case 300:
            setTiming(TSL2591_INTEGRATIONTIME_300MS);
            break;
        case 400:
            setTiming(TSL2591_INTEGRATIONTIME_400MS);
            break;
        case 500:
            setTiming(TSL2591_INTEGRATIONTIME_500MS);
            break;
        case 600:
            setTiming(TSL2591_INTEGRATIONTIME_600MS);
            break;
    }
    Serial.print  ("[luz] Timing:       ");
    Serial.print((getTiming() + 1) * 100, DEC); 
    Serial.println(" ms");
}

void Luz::read(void)
{
  delay(500);
  lum  = getFullLuminosity();
  delay(10);
  ir   = lum >> 16;
  full = lum & 0xFFFF;
  luminosity = calculateLux(full, ir);
} 

void Luz::switch_gain(int g){
    gain = g;
    switch(g)
    {
        case 1:
            setGain(TSL2591_GAIN_LOW);            
        break;
        case 2:
            setGain(TSL2591_GAIN_MED);
        break;
        case 3:
            setGain(TSL2591_GAIN_HIGH);
        break;
        case 4:
            setGain(TSL2591_GAIN_MAX); 
        break;
    }
}

void Luz::read_auto(void){
    read();
    if (full>h1){
        if (gain==3){
            switch_gain(2);
            read();
        }
        else if (gain==2){
            switch_gain(1);
            read();
        }
    }
    else if(full<h0){
        if (gain==1){
            switch_gain(2);
            read();
        }
        else if (gain==2){
            switch_gain(3);
            read();
        }
    }
}

uint32 Luz::read_factor(void){
  if (gain == 1){
      return 25*17;
  } else if (gain ==2){
      return 17;
  }
  else{
      return 1;
  }
}