#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

class Luz : public Adafruit_TSL2591
{
    
    int integration;  //100|200|300|400|500|600 (ms)
    uint32_t lum;
    uint32 h0 = 1300, h1 = 52430;

    public:
        Luz (): Adafruit_TSL2591{2591}{
            gain = 2; 
            integration = 500;
        };
        void info(void);
        void init(void);
        void read(void);
        void switch_gain(int g);
        void read_auto(void);
        uint32 read_factor(void);

        char name[12];
        int32_t version;
        int32_t sensor_id;
        float max_value, min_value, resolution;
        
        uint16_t ir, full;
        float luminosity;
        int gain;         //1:LOW | 2:MED | 3:HIGH | 4:MAX

};
