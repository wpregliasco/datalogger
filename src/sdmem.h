#pragma once
#include <Arduino.h>
#include "SdFat.h"

class SDmem
{
    // const uint8_t SOFT_MISO_PIN = 12;
    // const uint8_t SOFT_MOSI_PIN = 13;
    // const uint8_t SOFT_SCK_PIN  = 14;
    // const uint8_t SD_CS_PIN = 15;   
    SoftSpiDriver<12,13,14> softSpi;

    void fopen(String fname);
    void fclose(void);
    void fwrite(String s);
    int  save_period = 0;  //0:daily, 1:monthly, 2:yearly

    public:
        void init(unsigned long loop);
        void log(String date, String out);
        String ls(void);
            File32 file;
            SdFat32 sd;
};