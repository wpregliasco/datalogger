// IP Button
// Si está presionado entre mediciones,
// muestra el IP
// Hay que inicializarlo luego de inicializar la conexión WiFi
// Cableado:
//     cable 1: Di 
//     cable 2: GND

#include <Arduino.h>
#include "ipBtn.h"

void IPBtn::init(int ip_pin){
    pinMode(ip_pin, INPUT_PULLUP);
    pin = ip_pin;
}

void IPBtn::activate(void){
    pressed = 1;
}

void IPBtn::reset(void){
    pressed = 0;
}

boolean IPBtn::status(void){
    return digitalRead(pin)==LOW;
}

boolean IPBtn::longPress(void){
    // Lee el estado del botón en dos momentos. 
    // Si en ambos está presionado, lo da por presionado.
    delay(1000);
    if (IPBtn::status()){
        delay(1000);
        if (IPBtn::status()){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}