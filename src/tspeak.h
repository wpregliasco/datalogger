#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"

class Thingspeak
{
    WiFiClient  client;
    unsigned long Channel;
    const char * Key;
    public:
        void init(unsigned long ChannelNumber, const char* WriteAPIKey);
        int writeField(unsigned int field, float value);
        int publish(float var1, float var2, 
                    //float var3, float var4, 
                    float var5, float var6, 
                    float var7);
};