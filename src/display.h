#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

class Display : public Adafruit_SSD1306
{
    int  addr;

    public:
        Display(uint8_t address, uint8_t width, uint8_t height, uint8_t rst): Adafruit_SSD1306 {width, height, &Wire, (int8_t)rst}{
            addr = address;
        };
        void init(void);
        void reinit(void);
};