#ifdef DEBUG
#define OUT_INIT Serial.begin(115200)
#define OUT(x)   Serial.print(x)
#define OUTLN(x) Serial.println(x)
#else
#define OUT_INIT ;
#define OUT(x)   ;
#define OUTLN(x) ;
#endif

#include <Arduino.h>
#include "SdFat.h"
#include "sdmem.h"

void SDmem::init(unsigned long loop)
{
    OUT("[SD] Inicializando SD...");
    //if (!sd.begin(SdSpiConfig(SD_CS_PIN, DEDICATED_SPI, SD_SCK_MHZ(0), &softSpi))) {
    if (!sd.begin(SdSpiConfig(15, DEDICATED_SPI, SD_SCK_MHZ(0), &softSpi))) {    
        OUT(". Falla en la lectura de la tarjeta");
        sd.initErrorHalt();
        OUTLN(". Revisar SD.");
  }
    OUTLN(".listo.");

    if (loop<3*60){
        // dayly
        save_period = 0;
        OUTLN("[SD] Save file dayly.");
    } else if (loop<60*60){
        // monthñly
        save_period = 1;
        OUTLN("[SD] Save file monthly.");
    }
    else{
        // yearly
        save_period = 2;
        OUTLN("[SD] Save file yearly.");
    }
}

void SDmem::fopen(String fname)
{
    // OUTLN("\n>>"+fname+"<<");
    if (!file.open(fname.c_str(), O_APPEND | O_CREAT | O_WRONLY)) 
    {
        OUTLN("No se pudo abrir el archivo.");
        sd.errorHalt("falló la apertura del file.");
    }
}

void SDmem::fclose(void)
{
    file.close();
}

void SDmem::fwrite(String s)
{
    file.println(s);
}

void SDmem::log(String date, String out)
{
    if (save_period==0){
        // daily
        fopen(date+".csv");
    } else if (save_period==1){
        // monthly
        fopen(date.substring(0,7)+".csv");
    } else if (save_period==2){
        // yearly
        fopen(date.substring(0,4)+".csv");
    } else{
        // just in case
        fopen("data.csv");
    }
    fwrite(out);
    fclose();
}

String SDmem::ls(void){
    File32 dir;
    char fileName[20];
    String out="";

    if (!dir.open("/")) {
        sd.errorHalt(&Serial, "[SD] dir.open failed");
    }

    dir.rewind();

    while (file.openNext(&dir, O_READ)) {
        if (!file.isHidden()) {
            file.getName(fileName, sizeof(fileName));
            if (String(fileName).substring(String(fileName).lastIndexOf('.'))==".csv"){
                out = out + String(fileName) + "\n";
                //Serial.print(fileName);
                //Serial.write('\t'); Serial.print(file.fileSize()); Serial.println(F(" bytes"));
            }
        }
        file.close();
    }
    return out;
}