![](https://img.shields.io/badge/ver-1.0-green)  ![hver: prototipo](https://img.shields.io/badge/hver-prototipo-green)

<table width='100%'>
<TR>
<TD><img src='https://lh3.googleusercontent.com/pw/AL9nZEXpyYV9ti5YWDer58LuQ6ydUXhGZnWN_p7FefkPdTZkrxpp7Ir_rg37rvGhHdLDe91ohBEp7R1UXKtflvIN73EBYXMNpk5rxjKBC8rYpVGBx-NzPR4b4iO_27pRp8whdobJvpnvGHU3FOI9Di_ElZmVoA=w955-h715-no' width='100%'>
</TD>
<TD width=50% valign='top'>
<A HREF="https://thingspeak.com/channels/1693747" >
<IMG SRC='https://lh3.googleusercontent.com/pw/AM-JKLUz735nL2qa-pic4scD-VAHnPsVsggqK_7sbgqf_dFMKF1ASFDEzJlSZsDHSWA45IMM9yhvHMsHEWStU4O1JouS54iEne1Q931EVgDyMrSQTg1n3MxIjSNnc6sYd1SBjk52c9m_XLNL34du8exkuskFWg=w1012-h668-no' width='100%'>
(click para ver los datos en tiempo real)
</A>
</TD>
</TR>
</table>

# Datalogger

Datalogger hecho con con ESP8266.  

Entregada versión ![](https://img.shields.io/badge/ver-1.0-green) 

En esta versión, el equipo requiere energía permanente, conexión a WiFi y una cuenta 
para subir los datos a Thingspeak.

Realiza las operaciones:

* se conecta a un WiFi
* sincroniza la hora con un servidor NTP
* cada 1 minuto registra los valores de _hora, temperatura, humedad, luminancia_
* guarda los datos en una tarjeta SD
* los sube a un repositorio de Internet
* se pueden bajar los datos desde la red local

## Personalidad

Está diseñado para los Archivos del Poder Judicial de Bariloche.  
Los requisitos son:

* tener un costo menor que AR$ 10 000.- _(ajustable según inflación)_
* programación en C++ 
* cada dispositivo es un objeto de software

## Instalación

* Este repositorio tiene la estructura del un proyecto [`PlatformIO`](https://docs.platformio.org/en/latest/integration/ide/vscode.html) para el VSCode.  
  Antes de empezar tengo que tener instalada la extensión ([instrucciones](https://gitlab.com/wpregliasco/software-tips/-/wikis/Raspi/ESP8266#instalaci%C3%B3n-esp8266-nodemcu-v3))

* Clonar el repositorio git.    
  voy al directorio de proyectos adonde quiero instalar `dataloger`
  
  ```bash
  ~/misproyectos$ git clone git@gitlab.com:wpregliasco/datalogger.git
  ~/misproyectos$ cd datalogger
  ~/misproyectos/datalogger$ code . 
  ```

* `patformio.ini` (agregar lo que falte)  
  
  ```
  [env:nodemcuv2]
  platform = espressif8266
  board = nodemcuv2
  framework = arduino
  monitor_speed = 115200
  build_flags = 
    -D DEBUG
    -D LCD_DISPLAY
  ```
  
  _(La última línea habilita los mensajes por puerto serie)_

* Bibliotecas a instalar:
  
  * __WiFiManager__ <small>by tzapu</small>
  * __ArduinoJson__ <small>by Benoit Blanchon</small>
  * __NTPClient__ <small>by Fabrice Weinberg</small>
  * __Adafruit SSD1306__ <small>by Adafruit</small>
  * __Adafruit BusIO__ <small>by Adafruit</small>
  * __Adafruit GFX Library__ <small>by Adafruit</small>
  * __ThingSpeak__ <small>by MathWorks</small>
  * __SdFat__ <small>by Bill Greiman</small>
  * __Adafruit Unified Sensor__ <small>by Adafruit</small>
  * __Adafruit BMP085 Library__  <small>by Adafruit</small>

Para que la biblioteca `SdFat` funcione, hay que editar el archivo `.pio/libdeps/nodemcuv2/SdFat/src/SdFatConfig.h` y agregar:

```cpp
#define SPI_DRIVER_SELECT 2
```

Y con eso estamos listos para compilar y subir los códigos a la ESP.

## Documentación

La información está en diferentes documentos:

|                                                                                                   |                                                                      |
| ------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------- |
| [Programación de la ESP8266](https://gitlab.com/wpregliasco/software-tips/-/wikis/Raspi/ESP8266)  | ![status: ok](https://img.shields.io/badge/status-oK-green)          |
| [Diseño del Software](https://gitlab.com/wpregliasco/datalogger/-/wikis/Dise%C3%B1o-del-Software) | ![status: casi](https://img.shields.io/badge/status-oK-green)        |
| [Diseño del Hardware](https://gitlab.com/wpregliasco/datalogger/-/wikis/v1:-Dise%C3%B1o-del-hardware) | ![status: ok](https://img.shields.io/badge/status-oK-green)          |
| [Manual de Usuario](https://gitlab.com/wpregliasco/datalogger/-/wikis/v1:Manual-de-Usuario)     | ![status: development](https://img.shields.io/badge/status-oK-green) |

## Consultas

Contactar a _Willy Pregliasco_ (willy.pregliasco@gmail.com) pero tener en cuenta que mis conocimientos sobre el tema son los apenas necesarios para llevar esto adelante.   
Más expeditivo sería [agregar un Issue al proyecto](https://gitlab.com/wpregliasco/datalogger/-/issues).

## Plan de mejoras

* __25/03/2022__  Terminado el prototipo

* __v1.0__ 04/09/2022
  
  * posibilidad de bajar los datos por la red
  * entrada de los datos por menú
  * manual de usuario
  * tuve que eliminar el barómetro. Pasar del bus I2C a SPI.

* dispositivo autónomo:
  
  * agregar un scheduler y sacar el display para consumir menos energía 
  * agregar un rtc y no usar internet para medir

## Contribuir

Si tiene sugerencias problemas o contribuciones para hacer, la mejor forma es [escribiendo un issue](https://gitlab.com/wpregliasco/datalogger/-/issues). Al hacerlo hacer una descripción precisa problema para poder replicarlo e intentar una resolución.

## Licencia

Este repositorio es público y tiene licencia Creative Commons.
